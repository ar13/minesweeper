#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

void changeConsoleColor(void);
void displayMainMenu(void);
int getOption(int, int);
void clrscr(void);
void pressEnter(void);

#endif
