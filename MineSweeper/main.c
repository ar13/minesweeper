#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "functions.h"

/* global variables */
int r, c; //number of rows, columns
long long numberOfMines, numberOfMoves = 0, remainingFlags, totalTime = 0, timeTemp = 0;

/*score sheet structure*/
struct users{
    char name[100];
    double score;
};

/*saving files*/
FILE *sf,*sf2,*ss;

/*functions declaration*/
void goToScoreBoard();
void printGrid(char grid[r][c],long long startTime,long long moveTime);
void timer(long long startTime, long long moveTime);
void mines_Array(long long rowNumber,long long columnNumber,int minesArray[r][c]);
void open(char grid[r][c],int minesArray[r][c],long long coordinate1,long long coordinate2);
int winCheck(char grid[r][c], int r, int c);
void winGrid(char grid[r][c],int r,int c);
void saveVars();
void loadVars();
void saveArray(char grid[r][c],int minesArray[r][c]);
void loadArray(char grid[r][c],int minesArray[r][c]);
int compare(char savedName[100],char newName[100]);
void scoreSheet(double userScore);

int main() {
    changeConsoleColor();

    while(1) {
        displayMainMenu();
        int option;
        do {
            option = getOption(1, 3);
            if (option == 3) {
                goToScoreBoard();
                pressEnter();
                clrscr();
                displayMainMenu();
            }
        } while (option == 3);
        clrscr();
        if (option == 1) {
            printf("Number of rows(2-30): ");
            r = getOption(2, 30); /*get number of rows of the grid*/
            printf("Number of columns(2-30): ");
            c = getOption(2, 30); /*get number of columns of the grid*/
        } else if (option == 2) {
            loadVars(); /*load variables*/
            timeTemp = totalTime; /*saved game time to add to the time after loading*/
        }
        clrscr();
        int minesArray[r][c];
        char grid[r][c];
        numberOfMines = 1+(r*c)/10;
        /*
            play
        */
        if (option == 1) {
            remainingFlags = numberOfMines; /*initializing number of flags could be used*/
            /*printing the closed grid before doing the first move*/
            printf("Moves:%I64d F:%I64d Time: 00:00\n\n",numberOfMoves,remainingFlags);
            int i,j,k;
            for(i=0; i<=r; i++){
                for(j=0; j<=c; j++){
                    grid[i][j] = 'X';
                }
            }
            for(i=0; i<r; i++){
                for(j=0; j<c; j++){
                    if(c>=10){
                        printf(" %c | ",grid[i][j]);
                    }
                    else{
                        printf(" %c |",grid[i][j]);
                    }
                }
                printf(" %d",i+1);
                printf("\n");
                if(c<10){
                    for(k=0; k<4*c+3; k++){
                        printf("-");
                    }
                }
                else{
                    for(k=0; k<8+5*(c-1); k++){
                        printf("-");
                    }
                }
                printf("\n");
            }
            for(i=1; i<=c; i++){
                if(c<10){
                    printf(" %d |",i);
                }
                if(c>=10){
                    if(i<10){
                        printf(" %d | ",i);
                    }
                    else{
                        printf("%d | ",i);
                    }
                }
            }
            printf(" o");
            printf(" ");
        } else if (option == 2) {
            loadArray(grid,minesArray);
            printGrid(grid,1,1);
        }

        int status = 0; /* win 1, lose -1 or continue 0*/
        time_t startTime=time(NULL); /*storing the time when starting the game*/
        while (1) {
            long long rowNumber,columnNumber;
            do{ /*get coordinates of the cell the user want to do action on it*/
                printf("\n\nRow: ");
                rowNumber=getOption(1, 30);
            }while(rowNumber>r);
            do{
                printf("\nColumn: ");
                columnNumber=getOption(1, 30);
            }while(columnNumber>c);
            printf("\n1-Open 2-Flag 3-Not sure 4-Unmark 5-Save & Exit"); /*displaying options*/
            printf("\n\nOption: ");
            int optionB = getOption(1, 5); /*get option number*/
            switch(optionB){
            case 1 : /*if user pressed 1 for open cell*/
                if (numberOfMoves == 0) {
                    mines_Array(rowNumber,columnNumber,minesArray);
                }
                if(grid[rowNumber-1][columnNumber-1]!='F'){
                    if(minesArray[rowNumber-1][columnNumber-1]==0){
                        minesArray[rowNumber-1][columnNumber-1]=-1;
                        open(grid,minesArray,rowNumber,columnNumber);
                        grid[rowNumber-1][columnNumber-1] = ' ';
                        break;
                    }
                    else if(minesArray[rowNumber-1][columnNumber-1]==42){
                        grid[rowNumber-1][columnNumber-1] = '*';
                        status=-1;
                    }
                    else if(grid[rowNumber-1][columnNumber-1]!='X' && grid[rowNumber-1][columnNumber-1]!='F' && grid[rowNumber-1][columnNumber-1]!='?'){
                        long long x=rowNumber-1,y=columnNumber-1;
                        int i,j,temp=0;
                        for(i=x-1; i<=x+1; i++){
                            if((i<r) && (i>=0)){
                                for(j=y-1; j<=y+1; j++){
                                    if((j<c) && (j>=0)){
                                        if((i!=x)||(j!=y)){
                                            if (grid[i][j]=='F'){
                                                temp++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(temp==minesArray[x][y]){
                            for(i=x-1; i<=x+1; i++){
                                if((i<r) && (i>=0)){
                                    for(j=y-1; j<=y+1; j++){
                                        if((j<c) && (j>=0)){
                                            if(((i!=x)||(j!=y)) && (grid[i][j]!='F') && (grid[i][j]!='?')){
                                                if(minesArray[i][j]==0){
                                                    minesArray[i][j]=-1;
                                                    open(grid,minesArray,i+1,j+1);
                                                    grid[i][j]=' ';
                                                }
                                                else{
                                                    switch(minesArray[i][j]){
                                                    case 1:
                                                        grid[i][j]=49;
                                                        break;
                                                    case 2:
                                                        grid[i][j]=50;
                                                        break;
                                                    case 3:
                                                        grid[i][j]=51;
                                                        break;
                                                    case 4:
                                                        grid[i][j]=52;
                                                        break;
                                                    case 5:
                                                        grid[i][j]=53;
                                                        break;
                                                    case 6:
                                                        grid[i][j]=54;
                                                        break;
                                                    case 7:
                                                        grid[i][j]=55;
                                                        break;
                                                    case 8:
                                                        grid[i][j]=56;
                                                        break;
                                                    case 42:
                                                        grid[i][j]='*';
                                                        status = -1;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else{
                        switch(minesArray[rowNumber-1][columnNumber-1]){
                        case 1:
                            grid[rowNumber-1][columnNumber-1]=49;
                            break;
                        case 2:
                            grid[rowNumber-1][columnNumber-1]=50;
                            break;
                        case 3:
                            grid[rowNumber-1][columnNumber-1]=51;
                            break;
                        case 4:
                            grid[rowNumber-1][columnNumber-1]=52;
                            break;
                        case 5:
                            grid[rowNumber-1][columnNumber-1]=53;
                            break;
                        case 6:
                            grid[rowNumber-1][columnNumber-1]=54;
                            break;
                        case 7:
                            grid[rowNumber-1][columnNumber-1]=55;
                            break;
                        case 8:
                            grid[rowNumber-1][columnNumber-1]=56;
                            break;
                        }
                    }
                }
                break;
            case 2: /*if user pressed 2 for putting flag in a cell*/
                if(remainingFlags>0){
                    if(grid[rowNumber-1][columnNumber-1]=='X'){
                        grid[rowNumber-1][columnNumber-1]='F';
                        remainingFlags--;
                    }
                }
                break;
            case 3: /*if user pressed 3 for putting question mark in a cell*/
                if(grid[rowNumber-1][columnNumber-1]=='X'){
                    grid[rowNumber-1][columnNumber-1]='?';
                }
                break;
            case 4: /*if user pressed 4 for unmarking a cell*/
                if(grid[rowNumber-1][columnNumber-1]=='F'){
                    grid[rowNumber-1][columnNumber-1]='X';
                    remainingFlags++;
                }
                else if(grid[rowNumber-1][columnNumber-1]=='?'){
                    grid[rowNumber-1][columnNumber-1]='X';
                }
                break;
            case 5: /*if user pressed 5 to save current progress and exit the game*/
                saveVars();
                saveArray(grid,minesArray);
                exit(0);
            }
            numberOfMoves++; /*number of moves increases by 1 after every action */
            time_t moveTime=time(NULL); /*storing the time after every move*/
            printf("\n");
            if (status == -1) { /* player loses */
                int q,w;
                for(q=0; q<r; q++){
                    for(w=0; w<c; w++){
                        if(grid[q][w]=='*'){
                            grid[q][w]='!';
                        }
                        else if(grid[q][w]=='F'){
                            if(minesArray[q][w]==42){
                                grid[q][w]='*';
                            }
                            else{
                                grid[q][w]='-';
                            }
                        }
                        else if (minesArray[q][w]==42)
                            grid[q][w]= 'M';
                    }
                }
                printGrid(grid,(long long)startTime,(long long)moveTime);
                printf("\n\nYou Lose !");
                pressEnter();
                break;
            } else {
                status = winCheck(grid,r,c); /*check if the user wins*/
                if (status == 1) { /* player wins */
                    winGrid(grid,r,c);
                    long long finishTime=moveTime-startTime; /*adding 1 to finish time is here to make sure time is not 0*/
                    if(finishTime==0){
                        finishTime=1;
                    }
                    long long finalScore = (r*r*r*r*c*c*c*c)/(finishTime*numberOfMoves); /*calculating score*/
                    printGrid(grid,(long long)startTime,(long long)moveTime);
                    printf("\n\nYou Win !\nYour score is %I64d",finalScore);
                    scoreSheet((double) finalScore); /*save the winner name and his score*/
                    pressEnter();
                    break;
                } else { /* continue playing */
                    printGrid(grid,(long long)startTime,(long long)moveTime);
                }
            }
        }
        clrscr();
        numberOfMoves = 0;
        totalTime = 0;
        timeTemp = 0;
    }

    return 0;
}

void goToScoreBoard(){
    clrscr();
    printf("                => Score Board <=\n\n");
    int k=0,i,j;
    double t;
    char temp[100];
    struct users l[100];
    ss = fopen("files/ScoreSheet.txt","rb"); /*loading names with scores from score sheet file*/
    fread(&k,1,1,ss);
    for(i=0; i<k; i++){
        fread(&l[i], sizeof(struct users), 1, ss);
    }
    fclose(ss);
    /*arrange the names in descending order by scores*/
    for (i=0;i<k;i++){
        for (j=i+1;j<k;j++){
            if (l[i].score < l[j].score){
                t = l[i].score; /*swap the score*/
                l[i].score = l[j].score;
                l[j].score = t;
                strcpy(temp,l[i].name); /*swap the name*/
                strcpy(l[i].name,l[j].name);
                strcpy(l[j].name,temp);
            }
        }
    } /*printing the score board*/
    printf("\n\n| R |      score        |        Name\n----------------------------------------------------------");
    for (i=0;i<k;i++){
        printf("\n|%03d|     %08.0lf      |      %s",i+1,l[i].score,l[i].name);
    }
}

void printGrid(char grid[r][c],long long startTime,long long moveTime){
    clrscr();
    printf("Moves:%I64d F:%I64d Time: ",numberOfMoves,remainingFlags);
    timer(startTime,moveTime+1);
    printf("\n\n");
    int i,j,k;
    for(i=0; i<r; i++){
        for(j=0; j<c; j++){
            if(c>=10){
                printf(" %c | ",grid[i][j]);
            }
            else{
                printf(" %c |",grid[i][j]);
            }
        }
        printf(" %d",i+1);
        printf("\n");
        if(c<10){
            for(k=0; k<4*c+3; k++){
                printf("-");
            }
        }
        else{
            for(k=0; k<8+5*(c-1); k++){
                printf("-");
            }
        }
        printf("\n");
    }
    for(i=1; i<=c; i++){
        if(c<10){
            printf(" %d |",i);
        }
        if(c>=10){
            if(i<10){
                printf(" %d | ",i);
            }
            else{
                printf("%d | ",i);
            }
        }
    }
    printf(" o");
    printf(" ");
}

void timer(long long startTime, long long moveTime){
    totalTime = moveTime - startTime + timeTemp;
    long long  minute = totalTime / 60;
    long long second = totalTime % 60;
    printf("%.2I64d:%.2I64d",minute,second);
}

void mines_Array(long long rowNumber,long long columnNumber,int minesArray[r][c]){
    srand(time(NULL));
    int random1,random2;
    int x,y,i,j,z=0,temp,numberOfMines=1+(r*c)/10;
    while(z<numberOfMines){
        random1=rand();
        x=random1%r;
        random2=rand();
        y=random2%c;
        if((x!=rowNumber-1) || (y!=columnNumber-1)){
            if(minesArray[x][y]!=42){
                minesArray[x][y]=42;
                z++;
            }
        }
    }
    for(i=0; i<r; i++){
        for(j=0; j<c; j++){
            temp=0;
            if(minesArray[i][j]!=42){
                if(((i+1)<r && (i+1)>=0) && ((j)<c && (j)>=0)){
                    if(minesArray[i+1][j]==42){
                        temp++;
                    }
                }
                if(((i+1)<r && (i+1)>=0) && ((j+1)<c && (j+1)>=0)){
                    if(minesArray[i+1][j+1]==42){
                        temp++;
                    }
                }
                if(((i+1)<r && (i+1)>=0) && ((j-1)<c && (j-1)>=0)){
                    if(minesArray[i+1][j-1]==42){
                        temp++;
                    }
                }
                if(((i)<r && (i)>=0) && ((j-1)<c && (j-1)>=0)){
                    if(minesArray[i][j-1]==42){
                        temp++;
                    }
                }
                if(((i-1)<r && (i-1)>=0) && ((j-1)<c && (j-1)>=0)){
                    if(minesArray[i-1][j-1]==42){
                        temp++;
                    }
                }
                if(((i-1)<r && (i-1)>=0) && ((j)<c && (j)>=0)){
                    if(minesArray[i-1][j]==42){
                        temp++;
                    }
                }
                if(((i-1)<r && (i-1)>=0) && ((j+1)<c && (j+1)>=0)){
                    if(minesArray[i-1][j+1]==42){
                        temp++;
                    }
                }
                if(((i)<r && (i)>=0) && ((j+1)<c && (j+1)>=0)){
                    if(minesArray[i][j+1]==42){
                        temp++;
                    }
                }
                minesArray[i][j]=temp;
            }
        }
    }
}

void open(char grid[r][c],int minesArray[r][c],long long coordinate1,long long coordinate2){
    long long x,y;
    x=coordinate1-1;
    y=coordinate2-1;
    if (minesArray[x][y]>=1 && minesArray[x][y]<=8){
        return;
    }
    int i,j;
    for(i=x-1; i<=x+1; i++){
        if((i<r) && (i>=0)){
            for(j=y-1; j<=y+1; j++){
                if((j<c) && (j>=0)){
                    if(((i!=x)||(j!=y)) && ((grid[i][j]!='F') && (grid[i][j]!='?'))){
                        if (minesArray[i][j]==0){
                            grid[i][j]=' ';
                            minesArray[i][j]=-1;
                            open(grid,minesArray,i+1,j+1);
                        }
                        else{
                            switch(minesArray[i][j]){
                            case 1:
                                grid[i][j]=49;
                                break;
                            case 2:
                                grid[i][j]=50;
                                break;
                            case 3:
                                grid[i][j]=51;
                                break;
                            case 4:
                                grid[i][j]=52;
                                break;
                            case 5:
                                grid[i][j]=53;
                                break;
                            case 6:
                                grid[i][j]=54;
                                break;
                            case 7:
                                grid[i][j]=55;
                                break;
                            case 8:
                                grid[i][j]=56;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

int winCheck(char grid[r][c], int r, int c){
    int i,j,k=0;
    long long n = r*c - numberOfMines;
    for(i=0; i<r; i++){
        for(j=0; j<c; j++){
            if (grid[i][j]!='X' && grid[i][j]!='F' && grid[i][j]!='?' && grid[i][j]!='*'){
                k++;
            }
        }
    }
    if (k==n){
        return 1;
    }
    else{
        return 0;
    }
}

void winGrid(char grid[r][c],int r,int c){
    int i,j;
    for(i=0; i<r; i++){
        for(j=0; j<c; j++){
            if(grid[i][j]=='X'){
                grid[i][j]='F';
            }
        }
    }
}

void saveVars(){
    sf = fopen("files/saveFile.txt","wb");
    fwrite(&r,1, 1,sf);
    fwrite(&c,1, 1,sf);
    fwrite(&numberOfMoves,1,1,sf);
    fwrite(&remainingFlags,1,1,sf);
    fwrite(&totalTime,1,1,sf);
    fclose(sf);
}


void saveArray(char grid[r][c],int minesArray[r][c]){
    sf2 = fopen("files/saveFile2.txt","wb");
    int n,l;
    for (n = 0; n < r; n++){
        for(l=0; l<c; l++){
            fwrite(&grid[n][l], 1, 1, sf2);
            fwrite(&minesArray[n][l], 1, 10, sf2);
        }
    }
    fclose(sf2);
}

void loadVars(){
    sf = fopen("files/saveFile.txt","rb");
    fread(&r,1, 1,sf);
    fread(&c,1, 1,sf);
    fread(&numberOfMoves,1,1,sf);
    fread(&remainingFlags,1,1,sf);
    fread(&totalTime,1,1,sf);
    fclose(sf);
}

void loadArray(char grid[r][c],int minesArray[r][c]){
    sf2 = fopen("files/saveFile2.txt","rb");
    int n,l;
    for (n = 0; n < r; n++){
        for(l=0; l<c; l++){
            fread(&grid[n][l], 1, 1, sf2);
            fread(&minesArray[n][l], 1, 10, sf2);
        }
    }
    fclose(sf2);
}

int compare(char savedName[100],char newName[100]){
    int i;
    char n1[100], n2[100];
    strcpy(n1,savedName);
    strcpy(n2,newName);
    if (strlen(n1)!=strlen(n2)) return 0;
    for(i=0; i<strlen(n2); i++)
    {
        n2[i]=toupper(n2[i]);
        n1[i]=toupper(n1[i]);
    }
    i = strcmp(n2,n1);
    if (i==0) return 1;
    else return 0;
}
void scoreSheet(double userScore){
    int i,j=0,k=0;
    FILE *ss;
    struct users l[100];
    ss = fopen("files/ScoreSheet.txt","rb");
    fread(&k,1,1,ss);
    for(i=0; i<k; i++){
        fread(&l[i], sizeof(struct users), 1, ss);
    }
    fclose(ss);
    char userName[100];
    printf("\nEnter your name ,winner! : ");
    gets(userName);
    for(i=0; i<100; i++){
        if (l[i].score == 0) break;
        if (compare(l[i].name,userName)){
            l[i].score = l[i].score + userScore;
            j =1;
        }
    }
    if(j==0){
        k=i+1;
        strcpy(l[i].name,userName);
        l[i].score=userScore;
    }
    ss = fopen("files/ScoreSheet.txt","wb");
    fwrite(&k,1,1,ss);
    for(i=0; i<k; i++){
        fwrite(&l[i], sizeof(struct users), 1, ss);
    }
    fclose(ss);
}
