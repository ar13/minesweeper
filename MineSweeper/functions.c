#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"

void changeConsoleColor(void){
    system("COLOR 0A");
}

void displayMainMenu(void){
    printf("   *     *                  *****\n");
    printf("   **   ** * *    * ****** *     * *     * ****** ****** *****  ****** *****\n");
    printf("   * * * * * *    * *      *       *     * *      *      *    * *      *    *\n");
    printf("   *  *  * * * *  * *****   *****  *  *  * *****  *****  *    * *****  *    *\n");
    printf("   *     * * *  * * *            * * * * * *      *      *****  *      *****\n");
    printf("   *     * * *   ** *      *     * **   ** *      *      *      *      *   *\n");
    printf("   *     * * *    * ******  *****  *     * ****** ****** *      ****** *    *\n");
    printf("\n1-New game   2-Load game   3-Score board\n");
    printf("\nSelect option: ");
}

int getOption(int start, int end){
    long long number;
    char buffer;
    int flag;
    do{
        flag = scanf("%I64d%c", &number, &buffer);
        while (flag !=2 || buffer!='\n'){
            pressEnter();
            printf("%s","Wrong input!\nSelect option: ");
            flag = scanf("%I64d%c", &number, &buffer);
        }
        if(number < start || number > end){
            printf("%s","Wrong input!\nSelect option: ");
        }
        else{
            break;
        }
    }
    while(1);
    return number;
}

void clrscr(){
    system("@cls||clear");
}

void pressEnter(void){
    printf("\n\n\n\n\nPress Enter to go back ");
    while (getchar()!='\n');
    return;
}
